using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Extensions
{
    public static class Enumerable<T> where T : BaseEntity
    {
        public static IEnumerable<T> Replace(IEnumerable<T> data, Guid id, T newEntity) 
        {
            var list = data.ToList();
            var index = list.FindIndex(d => d.Id == id);
            if (index < 0)
                return null;
            list.RemoveAt(index);
            if (newEntity.Id != id)
                newEntity.Id = id;
            list.Insert(index, newEntity);
            return list;
        }
    }
}