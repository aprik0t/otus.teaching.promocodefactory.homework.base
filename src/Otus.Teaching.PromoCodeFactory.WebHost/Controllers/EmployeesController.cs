﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController: ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IMapper _mapper;
        private readonly IRepository<Role> _roleRepository;

        public EmployeesController(IRepository<Employee> employeeRepository,
            IMapper mapper, IRepository<Role> roleRepository)
        {
            _employeeRepository = employeeRepository;
            _mapper = mapper;
            _roleRepository = roleRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeDtoLite>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeDtoLite
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeDto>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            return _mapper.Map<EmployeeDto>(employee);
        }

        /// <summary>
        /// Создать сотрудника
        /// </summary>
        /// <param name="employeeDto"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> CreateEmployeeAsync([FromBody] EmployeeDto employeeDto)
        {
            if (InvalidRolesDetected(employeeDto, out var invalidRoles))
                return BadRequest(BuildRolesErrorMessage(invalidRoles));
            var validationResult = employeeDto.Validate();
            if (!validationResult.Success)
                return BadRequest(validationResult.Errors);
            
            var employeeData = _mapper.Map<Employee>(employeeDto);
            var createdEmployeeId = await _employeeRepository.CreateAsync(employeeData);
            return createdEmployeeId == Guid.Empty 
                ? StatusCode((int) HttpStatusCode.InternalServerError, "Ошибка при создании записи.") 
                : Ok(createdEmployeeId);
        }
        
        /// <summary>
        /// Обновить данные о сотруднике по Id
        /// </summary>
        /// <returns></returns>
        [HttpPost("{id:guid}")]
        public async Task<IActionResult> UpdateEmployeeAsync(Guid id, [FromBody] EmployeeDto employeeDto)
        {
            if (InvalidRolesDetected(employeeDto, out var invalidRoles))
                return BadRequest(BuildRolesErrorMessage(invalidRoles));
            var validationResult = employeeDto.Validate();
            if (!validationResult.Success)
                return BadRequest(validationResult.Errors);

            var employee = await _employeeRepository.GetByIdAsync(id);
            if (employee == null)
                return NotFound();
            
            var employeeData = _mapper.Map<Employee>(employeeDto);
            var updatedEmployee = await _employeeRepository.UpdateAsync(id, employeeData);
            var updatedEmployeeDto = _mapper.Map<EmployeeDto>(updatedEmployee);
            return updatedEmployeeDto == null 
                ? StatusCode((int) HttpStatusCode.InternalServerError, $"Ошибка при обновления записи {id}.") 
                : Ok(updatedEmployeeDto);
        }

        /// <summary>
        /// Удалить сотрудника по Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteEmployeeAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);
            if (employee == null)
                return NotFound();
            
            await _employeeRepository.DeleteAsync(id);
            return Ok();
        }
        
        #region Бизнес-логика, которую лучше вынести в сервисы
        private bool InvalidRolesDetected(EmployeeDto dto, out ICollection<Guid> invalidRoles)
        {
            if (!dto.Roles.Any())
                invalidRoles = new List<Guid>();
            else
            {
                var roles = _roleRepository.GetAllAsync().GetAwaiter().GetResult();
                var roleIds = roles.Select(r => r.Id).ToList();
                invalidRoles = dto.Roles.Except(roleIds).ToList();
            }
            
            return invalidRoles.Any();
        }
        private static string BuildRolesErrorMessage(ICollection<Guid> roles)
        {
            var many = roles.Count > 1;
            return
                $"Данн{(many ? "ые" : "ая")} рол{(many ? "и" : "ь")} отсутству{(many ? "ю" : "е")}т в источнике данных: " +
                string.Join(", ", roles.Select(r => $"'{r:D}'"));
        }
        #endregion
    }
}