namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class ValidationResult
    {
        public bool Success { get; set; }
        public string[] Errors { get; set; }
        
        private ValidationResult(){}

        public static ValidationResult Ok => new ValidationResult { Success = true };
        
        public static ValidationResult Fail(string[] errors) => new ValidationResult
        {
            Success = false,
            Errors = errors
        };
    }
}