﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class EmployeeDto
    {
        public Guid Id { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public List<Guid> Roles { get; set; }
        public int AppliedPromocodesCount { get; set; }

        public ValidationResult Validate()
        {
            if (string.IsNullOrWhiteSpace(FullName))
                return ValidationResult.Fail(new []{ "Имя не может быт пустым." });
            var splitted = FullName.Split(' ', StringSplitOptions.RemoveEmptyEntries);
            if (splitted.Length != 2) 
                return ValidationResult.Fail(new []{ $"Фамилия и/или имя не задано: '{FullName.Trim()}'." });
            return string.IsNullOrWhiteSpace(Email) 
                ? ValidationResult.Fail(new []{ "Email не может быт пустым." }) 
                : ValidationResult.Ok;
        }
    }
}