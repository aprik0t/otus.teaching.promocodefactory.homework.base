using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.WebHost.MappingProfiles;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Extensions
{
    public static class ServicesExtension
    {
        public static IServiceCollection AddAutomapper(this IServiceCollection services)
        {
            var mapConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile<AwesomeMappingProfile>();
            });
            var mapper = mapConfig.CreateMapper();
            services.AddSingleton(mapper);
            return services;
        }
    }
}