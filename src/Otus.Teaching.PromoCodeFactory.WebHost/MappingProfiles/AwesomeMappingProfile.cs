using System;
using System.Linq;
using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.MappingProfiles
{
    public class AwesomeMappingProfile: Profile
    {
        public AwesomeMappingProfile()
        {
            CreateMap<Employee, EmployeeDto>();
            CreateMap<EmployeeDto, Employee>()
                .ForMember(s => s.FirstName, 
                    o => o.MapFrom(t => 
                        t.FullName.Split(" ", StringSplitOptions.RemoveEmptyEntries).FirstOrDefault()))
                .ForMember(s => s.LastName, 
                    o => o.MapFrom(t => 
                        t.FullName.Split(" ", StringSplitOptions.RemoveEmptyEntries)[1]));
            CreateMap<Role, RoleDto>();
            CreateMap<RoleDto, Role>();
        }
    }
}