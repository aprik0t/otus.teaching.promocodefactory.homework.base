﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Extensions;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>: IRepository<T>
        where T: BaseEntity
    {
        private readonly FakeDbContext<T> _db;
        
        public InMemoryRepository(FakeDbContext<T> db)
        {
            _db = db;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(_db.Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(_db.Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<Guid> CreateAsync(T entity)
        {
            entity.Id = Guid.NewGuid();
            _db.Data = _db.Data.Append(entity).ToList();
            return Task.FromResult(entity.Id);
        }

        public Task<T> UpdateAsync(Guid id, T newEntity)
        {
            _db.Data = Enumerable<T>.Replace(_db.Data, id, newEntity);
            return Task.FromResult(newEntity);
        }

        public Task DeleteAsync(Guid id)
        {
            _db.Data = _db.Data.Where(d => d.Id != id).ToList();
            return Task.CompletedTask;
        }
    }
}