using System;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class FakeDbContext<T> where T: BaseEntity
    {
        public FakeDbContext(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public IEnumerable<T> Data { get; set; }
    }
}